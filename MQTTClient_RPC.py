####!/usr/bin/python

import random
import os
import time
import threading
#import urlparse as urlparse
#import ConfigParser as ConfigParser
import paho.mqtt.client as mqtt
import json
from faker import Faker
import datetime
import errno
from threading import Timer

class MQTTClientRPC(threading.Thread):
    def __init__(self, url, user, pwd, clientID, clientNum, msgNum, sleepTime, rootTopic, token):
        threading.Thread.__init__(self)
        print("sleepTime: " + str(sleepTime))
        self.url = url
        self.user = user
        self.pwd = pwd
        self.clientID = clientID
        self.clientNum = clientNum
        self.msgNum = msgNum
        self.sleepTime = sleepTime
        self.rootTopic = rootTopic
        self.counter=0
        self.token = token

    def on_connect(self, mqttc, obj, flags, rc):
        print("rc: " + str(rc))
	
    def on_message(self, mqttc, obj, msg):
 #       self.lock.acquire()
        print("MESSAGES: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
 #       self.lock.release()

    def on_message_reqs(self, mqttc, obj, msg):
#        self.lock.acquire()
        self.sleepTime = int(msg.payload)
        print("MESSAGES: " + str(self.clientID) + str(self.clientNum) + " publish time has been updated to " + str(msg.payload) + " seconds")
#        self.lock.release()
		
    def on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: " + str(mid) + " " + str(granted_qos))

    def on_log(self, mqttc, obj, level, string):
        print(string)
	
    def run(self): 
        def generateAndSaveData(self, mqttc, guid, pubTopic, fake):
            
            # Create directory and filename strings
            currentDT = datetime.datetime.now()
            dayStringFormat = str(currentDT.year) + "-" + str(currentDT.month) + "-" + str(currentDT.day)
            hourStringFormat = str(currentDT.hour)
            minuteStringFormat = str(currentDT.minute)
            
            # Create path and filename
            filename = "data/" + guid + "/" + dayStringFormat + "/" + hourStringFormat + "/" + minuteStringFormat + ".json"
            directory = os.path.dirname(filename)

            # Create directory if it does not exist
            if not os.path.exists(os.path.dirname(directory)):
                try:
                    os.makedirs(directory, 0o777)
                except OSError as exc: # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise

            # Open file to start writing
            file = open(filename, 'w')

            if self.msgNum > 0:

                for i in range(self.msgNum):
                    
                    msg = "MQTT Client RPC Testing - msgNum = " + str(self.sleepTime) + " : " + str(i) + ", " + str(time.time())
                    mqttc.publish(pubTopic, msg)
                    self.counter += 1
                    time.sleep(1)

                    # Generate data
                    data = {}
                    data['id'] = guid
                    data['latitude'] = str(fake.latitude())
                    data['longitude'] = str(fake.longitude())
                    data['temperature'] = fake.random_int(min=0, max=60)
                    json_data = json.dumps(data)
                    
                    print(pubTopic + "/" + msg)
                    
                    # Sending data to ThingsBoard
                    mqttc.publish('v1/devices/me/telemetry', json_data, 1)

                    # Write to file
                    file.write("%s\n" % json_data)

                # Close the file
                file.close()
                
                # Call function again
                generateAndSaveData(self, mqttc, guid, pubTopic, fake)

                mqttc.disconnect()
                mqttc.loop_stop()
                
            else:
                while True:
                    msg = "MQTT Client RPC Testing: " + str(time.time())
                    mqttc.publish(pubTopic, msg)
                    self.counter += 1
                    time.sleep(self.sleepTime)
                    print(pubTopic + "/" + msg)
           

        # MQTT client publish data at 1Hz (default). Update rate to be changed based on RPC message
        pubTopic = self.rootTopic + "/" + str(self.clientID) + str(self.clientNum) + "/data"
        subTopic = self.rootTopic + "/" + str(self.clientID) + str(self.clientNum) + "/reqs"
        responseTopic = subTopic + "/response"

        mqttc = mqtt.Client(str(self.clientID)+str(self.clientNum), clean_session=True)	##create MQTT client instance with clean session

#        mqttc.on_connect = self.on_connect
        mqttc.on_message = self.on_message
        mqttc.message_callback_add(subTopic, self.on_message_reqs)
#        mqttc.on_publish = self.on_publish
#        mqttc.on_subscribe = self.on_subscribe 
#        mqttc.on_log = self.on_log

        mqttc.username_pw_set(self.token)
        mqttc.disconnect()
        mqttc.connect(self.url.hostname, self.url.port, 60)

        mqttc.loop_start()	# start loop to process received messages
        mqttc.subscribe(subTopic)
        mqttc.subscribe(responseTopic)
        
        fake = Faker()

        # Create fake guid
        guid = str(fake.uuid4())

        pubTopic = self.rootTopic + "/" + str(self.clientID) + str(self.clientNum) + "/data"

        generateAndSaveData(self, mqttc, guid, pubTopic, fake)
        
        mqttc.disconnect()
        mqttc.loop_stop()
        print(self.counter)