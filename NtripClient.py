#!/usr/bin/env python

import socket
import sys
import datetime
import base64
import time
import ssl

version=0.1
useragent="FiPy NtripClient/%.1f" % version

# reconnect parameter (fixed values):
factor=2 # How much the sleep time increases with each failed attempt
maxConnectTime=0
maxReconnect=100
sleepTime=1 # So the first one is 1 second
maxReconnectTime=300

class NtripClient(object):
    def __init__(self,
                 buffer=1024,
                 out=sys.stdout,
                 user="guest:test",			#Ntrip Caster user and password
                 port=2101,
                 caster="203.101.226.126",	#Ntrip Caster ip
                 mountpoint="/CLEV4",		#Ntrip Caster Mountpoint
                 ssl=False,
                 verbose=False,
                 V2=True,
                 ):
        self.buffer=buffer
        self.out=out
        self.user=base64.b64encode(user)
        self.port=port
        self.caster=caster
        self.mountpoint=mountpoint
        self.verbose=verbose
        self.ssl=ssl
        self.V2=V2
        self.maxConnectTime=maxConnectTime
        self.socket=None

    def getMountPointString(self):
        mountPointString = "GET %s HTTP/1.0\r\nUser-Agent: %s\r\nAuthorization: Basic %s\r\n" % (self.mountpoint, useragent, self.user)
        if self.V2:
           mountPointString+="Ntrip-Version: Ntrip/2.0\r\n"
        mountPointString+="\r\n"
        if self.verbose:
           print mountPointString
        return mountPointString

    def readData(self):
        reconnectTry=1
        sleepTime=1
        if maxConnectTime > 0 :
            EndConnect=datetime.timedelta(seconds=maxConnectTime)
        try:
            while reconnectTry<=maxReconnect:
                found_header=False
                if self.verbose:
                    sys.stderr.write('Connection {0} of {1}\n'.format(reconnectTry,maxReconnect))

                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                if self.ssl:
                    self.socket=ssl.wrap_socket(self.socket)

                error_indicator = self.socket.connect_ex((self.caster, self.port))
                if error_indicator==0:
                    sleepTime = 1
                    connectTime=datetime.datetime.now()

                    self.socket.settimeout(10)
                    self.socket.sendall(self.getMountPointString())
                    while not found_header:
                        casterResponse=self.socket.recv(4096) #All the data
                        header_lines = casterResponse.split("\r\n")

                        for line in header_lines:
                            if line=="":
                                if not found_header:
                                    found_header=True
                                    if self.verbose:
                                        sys.stderr.write("End Of Header"+"\n")
                            else:
                                if self.verbose:
                                    sys.stderr.write("Header: " + line+"\n")

                        for line in header_lines:
                            if line.find("SOURCETABLE")>=0:
                                sys.stderr.write("Mount point does not exist")
                                sys.exit(1)
                            elif line.find("401 Unauthorized")>=0:
                                sys.stderr.write("Unauthorized request\n")
                                sys.exit(1)
                            elif line.find("404 Not Found")>=0:
                                sys.stderr.write("Mount Point does not exist\n")
                                sys.exit(2)

                    data = "Initial data"
                    while data:
                        try:
                            data=self.socket.recv(self.buffer)
                            print(data)

                            if not data:
                               self.out.write(str(time.time())+', server disconnect')
                               break
                            print(data)

                            if maxConnectTime :
                                if datetime.datetime.now() > connectTime+EndConnect:
                                    if self.verbose:
                                        sys.stderr.write("Connection Timed exceeded\n")
                                    sys.exit(0)

                        except socket.timeout:
                            if self.verbose:
                                print('Connection TimedOut\n')
                            data=False
                        except socket.error:
                            if self.verbose:
                                print('Connection Error\n')
                            data=False

                    if self.verbose:
                        print('Closing Connection\n')
                    self.socket.close()
                    self.socket=None

                    if reconnectTry < maxReconnect :
                        print( "%s No Connection to NtripCaster.  Trying again in %i seconds\n" % (datetime.datetime.now(), sleepTime))
                        time.sleep(sleepTime)
                        sleepTime *= factor

                        if sleepTime>maxReconnectTime:
                            sleepTime=maxReconnectTime

                    reconnectTry += 1					
                else:
                    self.socket=None
                    if self.verbose:
                        print "Error indicator: ", error_indicator

                    if reconnectTry < maxReconnect :
                        sys.stderr.write( "%s No Connection to NtripCaster.  Trying again in %i seconds\n" % (datetime.datetime.now(), sleepTime))
                        time.sleep(sleepTime)
                        sleepTime *= factor
                        if sleepTime>maxReconnectTime:
                            sleepTime=maxReconnectTime
                    reconnectTry += 1

        except KeyboardInterrupt:
            if self.socket:
                self.socket.close()
            sys.exit()

if __name__ == '__main__':

    n = NtripClient()

    try:
        n.readData()
    finally:
        print("ending Ntrip Client")
