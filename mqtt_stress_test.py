#!/usr/bin/env python

import os
import time
import urlparse as urlparse
import ConfigParser as ConfigParser
import MQTTClient_RPC as client
import Queue as Queue
import threading

clients = []

def get_url(cp):
    url_str = cp.get('MQTT','url')
    if url_str.find('mqtt://') < 0:
        url_str = 'mqtt://' + url_str
    print("Trying url: " + url_str)
    url = urlparse.urlparse(url_str)
    return url

def createClients(url, user, pwd, clientID, clientNum, msgNum, sleepTime, rootTopic, token):
    for i in range(clientNum):
        t = client.ThreadClient(url, user, pwd, clientID, i, msgNum, sleepTime, rootTopic, token)
        t.setDaemon(True)
        clients.append(t)
        t.start()

def main():
    default_cfg = {
        'url'        : 'localhost:8080',
        'user'       : 'qut',
        'pwd'        : 'qut',
        'clientID'   : 'mqttSim',
        'clientNum'  : '1',
        'msgNum'     : '1',
        'sleepTime'  : '30',
        'rootTopic'      : '/mqttSim',
        }

    print('Config read from: ' + os.path.splitext(__file__)[0] + '.ini')
    cp = ConfigParser.SafeConfigParser(default_cfg)
    cp.read(os.path.splitext(__file__)[0] + '.ini')

    url = get_url(cp)
    user = cp.get('MQTT','user')
    pwd  = cp.get('MQTT','pass')
    token = cp.get('MQTT','token')
    clientID = cp.get('MESSAGES', 'clientID')
    clientNum = cp.getint('MESSAGES','clientNum')
    msgNum= cp.getint('MESSAGES', 'msgNum')
    sleepTime = cp.getfloat('MESSAGES', 'sleepTime')
    rootTopic = cp.get('MESSAGES', 'rootTopic')

    print (str(clientNum) +" clients (threads) will be created")

    try:
        for i in range(clientNum):
            t = client.MQTTClientRPC(url, user, pwd, clientID, i, msgNum, sleepTime, rootTopic, token)
            t.setDaemon(True)
            clients.append(t)
            clients[i].start()
        while True: time.sleep(10)
    except (KeyboardInterrupt, SystemExit):
        print('\n! Received keyboard interrupt, quitting threads. \n')

if __name__ == '__main__':
    main()